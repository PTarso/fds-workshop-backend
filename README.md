# Fábrica de Software: Workshop de Backend

### [Ir para Linha do Tempo](docs/linha-do-tempo.md)

### [Ver vídeo da instalação das ferramentas de desenvolvimento](https://youtu.be/zF5xD40EWvA)

### [Ver tutorial de criação de um ambiente virtualizado](docs/extras/ambiente-virtual.md)

### [Ver comandos do terminal Windows](docs/extras/comandos-terminal-windows.md)

### [Ver comandos do git](docs/extras/comandos-git.md)

### [Ver tutorial para criar um tópico (issue)](docs/extras/criar-issue.md)

---

## Curso de Django para iniciantes
Curso da Udemy de Django que foi disponibilizado para os imersionistas do semestre passado (2020.1).

**Realize o download através do Google Drive:** https://drive.google.com/folderview?id=13NYHaEz5Lo8gVsyiKThspih4MYBWE9fV

**Mensagem do veterano Cayo através do Whatsapp

## Discord

**Servidor no Discord:** https://discord.gg/7cXz65d
